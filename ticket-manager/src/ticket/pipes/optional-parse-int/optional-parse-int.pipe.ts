import {
  ArgumentMetadata,
  Injectable,
  Optional,
  ParseIntPipe,
  ParseIntPipeOptions,
  PipeTransform,
} from '@nestjs/common';

@Injectable()
export class OptionalParseIntPipe implements PipeTransform {
  private readonly parseInt: ParseIntPipe;
  constructor(@Optional() options?: ParseIntPipeOptions) {
    this.parseInt = new ParseIntPipe(options);
  }

  async transform(value: any, metadata: ArgumentMetadata): Promise<number | undefined> {
    if (value === null || value === undefined || value === '') {
      return undefined;
    }

    return this.parseInt.transform(value, metadata);
  }
}
