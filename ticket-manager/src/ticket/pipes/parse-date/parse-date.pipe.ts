import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class ParseDatePipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata): Date | undefined {
    if (!value) {
      return undefined;
    }

    const date = new Date(value);

    if (date.valueOf() === NaN) {
      return undefined;
    }

    return date;
  }
}
