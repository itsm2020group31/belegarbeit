import { Body, Controller, Get, Inject, Param, ParseIntPipe, Post, Query, Redirect, Render } from '@nestjs/common';
import { CreateTicket } from '@src/ticket/model/CreateTicket.dto';
import { Ticket } from '@src/ticket/model/Ticket.dto';
import { UpdateTicket } from '@src/ticket/model/UpdateTicket.dto';
import { OptionalParseIntPipe } from '@src/ticket/pipes/optional-parse-int/optional-parse-int.pipe';
import { ParseDatePipe } from '@src/ticket/pipes/parse-date/parse-date.pipe';
import { ServiceNowService } from '@src/ticket/services/service-now/service-now.service';

@Controller('ticket')
export class TicketController {
  @Inject()
  serviceNowService: ServiceNowService;

  @Get('new')
  @Render('ticket/new')
  getCreateTicket(): object {
    return {};
  }

  @Get()
  @Render('ticket/ticket')
  async getAllTickets(
    @Query('number') number?: string,
    @Query('start-date', ParseDatePipe) startDate?: Date,
    @Query('end-date', ParseDatePipe) endDate?: Date,
    @Query('state', OptionalParseIntPipe) state?: number,
  ): Promise<{ tickets: Ticket[] }> {
    return {
      tickets: await this.serviceNowService.getAllTickets(
        {
          number,
          startDate,
          endDate,
          state
        }
      ),
    };
  }

  @Post()
  @Render('ticket/created')
  async createTicket(
    @Body() createTicket: CreateTicket,
  ): Promise<{ ticket: Ticket }> {
    return {
      ticket: await this.serviceNowService.createTicket(createTicket),
    };
  }

  @Post(':id/delete')
  @Redirect('/ticket', 302)
  async deleteTicket(
    @Param('id') id: string,
  ): Promise<{}> {
    await this.serviceNowService.deleteTicket(id);
    return {};
  }

  @Get(':id/update')
  @Render('ticket/update')
  async showUpdateTicket(
    @Param('id') id: string,
  ): Promise<{}> {
    const ticket = await this.serviceNowService.getTicket(id);
    return { ticket };
  }

  @Post(':id/update')
  @Redirect('/ticket', 302)
  async updateTicket(
    @Param('id') id: string,
    @Body() updateTicket: UpdateTicket,
  ): Promise<{}> {
    await this.serviceNowService.updateTicket(id, updateTicket);
    return {};
  }
}
