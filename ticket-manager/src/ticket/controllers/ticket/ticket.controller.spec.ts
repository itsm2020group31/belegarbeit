import { Kentan } from '@kentan-official/core';
import { Test, TestingModule } from '@nestjs/testing';
import { TicketController } from '@src/ticket/controllers/ticket/ticket.controller';
import { ForCreateTicket } from '@src/ticket/model/ForCreateTicket.sketch';
import { ForTicket } from '@src/ticket/model/ForTicket.sketch';
import { ServiceNowService } from '@src/ticket/services/service-now/service-now.service';
import { stub, SinonStubbedInstance } from 'sinon';

describe('TicketController', () => {
  let controller: TicketController;
  let serviceNowService: SinonStubbedInstance<ServiceNowService>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TicketController],
      providers: [
        {
          provide: ServiceNowService,
          useFactory: function () {
            return stub(new ServiceNowService(null, null));
          },
        },
      ],
    }).compile();

    controller = module.get<TicketController>(TicketController);
    serviceNowService = module.get<
      ServiceNowService,
      SinonStubbedInstance<ServiceNowService>
    >(ServiceNowService);

    serviceNowService.getAllTickets.returns(Promise.resolve(Kentan.sketch(ForTicket).take(5).models()));
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('new ticket', () => {
    it('should not pass any parameters to the template', () => {
      expect(controller.getCreateTicket()).toEqual({});
    });
  });

  describe('create ticket', () => {
    it('should pass pass the create ticket request to the service', async () => {
      const createTicketBody = Kentan.sketch(ForCreateTicket).model();
      await controller.createTicket(createTicketBody);

      expect(serviceNowService.createTicket.calledOnce).toBeTruthy();
      expect(serviceNowService.createTicket.lastCall.firstArg).toEqual(
        createTicketBody,
      );
    });

    it('should pass the created ticket to the template', async () => {
      const ticket = Kentan.sketch(ForTicket).model();
      serviceNowService.createTicket.returns(Promise.resolve(ticket));

      expect(
        await controller.createTicket(Kentan.sketch(ForCreateTicket).model()),
      ).toEqual({ ticket });
    });
  });

  describe('get all tickets', () => {
    it('should call the ServiceNow service', async () => {
      await controller.getAllTickets();

      expect(serviceNowService.getAllTickets.calledOnce).toBeTruthy();
    });

    it('should pass the returned tickets to the template', async () => {
      const tickets = Kentan.sketch(ForTicket).take(5).models();
      serviceNowService.getAllTickets.returns(Promise.resolve(tickets));

      expect(await controller.getAllTickets()).toEqual({ tickets });
    });
  });
});
