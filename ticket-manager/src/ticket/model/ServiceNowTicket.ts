export interface ServiceNowTicket {
  sys_id?: string;
  short_description?: string;
  description?: string;
  number?: string;
  state?: string;
  opened_at?: string;
}
