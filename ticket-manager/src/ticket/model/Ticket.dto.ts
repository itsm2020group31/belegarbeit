export class Ticket {
  constructor(
    id: string,
    shortDescription?: string,
    description?: string,
    number?: string,
    state?: number,
    openedAt?: Date,
  ) {
    this.id = id;
    this.shortDescription = shortDescription;
    this.description = description;
    this.number = number;
    this.state = state;
    this.openedAt = openedAt;
  }

  id: string;
  shortDescription: string;
  description: string;
  number: string;
  state: number;
  openedAt: Date;
}
