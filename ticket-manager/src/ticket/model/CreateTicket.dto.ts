import { IsNotEmpty } from 'class-validator';

export class CreateTicket {
  @IsNotEmpty()
  shortDescription: string;
  @IsNotEmpty()
  description: string;
}
