import { Sketch } from '@kentan-official/core';
import { ServiceNowTicket } from '@src/ticket/model/ServiceNowTicket';
import { MockData } from '@testing/mock-data';

export class ForServiceNowTicket extends Sketch<ServiceNowTicket> {
  constructor() {
    super({
      sys_id: MockData.string(),
      description: MockData.sentence({ punctuation: false, words: 10 }),
      short_description: MockData.sentence({ punctuation: false, words: 5 }),
      number: MockData.integer().toString(),
      state: MockData.pickset(['1', '2', '3', '4'], 1)[0],
      opened_at: MockData.date().toDateString(),
    });
  }
}
