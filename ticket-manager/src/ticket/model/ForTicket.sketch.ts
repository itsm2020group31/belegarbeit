import { Constructable, Sketch } from '@kentan-official/core';
import { Ticket } from '@src/ticket/model/Ticket.dto';
import { MockData } from '@testing/mock-data';

export class ForTicket extends Sketch<Ticket> {
  constructor() {
    super(Ticket as Constructable<Ticket>, {
      id: MockData.string(),
      description: MockData.sentence({ punctuation: false, words: 10 }),
      shortDescription: MockData.sentence({ punctuation: false, words: 5 }),
      number: MockData.integer().toString(),
      state: MockData.pickset([1, 2, 3, 4, 5, 6, 7], 1)[0],
      openedAt: MockData.date(),
    });
  }
}
