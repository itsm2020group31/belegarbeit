import { Sketch } from '@kentan-official/core';
import { CreateTicket } from '@src/ticket/model/CreateTicket.dto';
import { MockData } from '@testing/mock-data';

export class ForCreateTicket extends Sketch<CreateTicket> {
  constructor() {
    super(CreateTicket, {
      description: MockData.sentence({ punctuation: false, words: 10 }),
      shortDescription: MockData.sentence({ punctuation: false, words: 5 }),
    });
  }
}
