import { Module, HttpModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { OptionalParseIntPipe } from '@src/ticket/pipes/optional-parse-int/optional-parse-int.pipe';
import { ParseDatePipe } from '@src/ticket/pipes/parse-date/parse-date.pipe';
import { TicketController } from './controllers/ticket/ticket.controller';
import { ServiceNowService } from './services/service-now/service-now.service';

@Module({
  imports: [HttpModule, ConfigModule],
  controllers: [TicketController],
  providers: [ServiceNowService, ParseDatePipe, OptionalParseIntPipe],
})
export class TicketModule {}
