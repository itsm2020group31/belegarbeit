import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { CreateTicket } from '@src/ticket/model/CreateTicket.dto';
import { ServiceNowTicket } from '@src/ticket/model/ServiceNowTicket';
import { Ticket } from '@src/ticket/model/Ticket.dto';
import { UpdateTicket } from '@src/ticket/model/UpdateTicket.dto';

@Injectable()
export class ServiceNowService {
  constructor(private http: HttpService, private config: ConfigService) {
  }

  get incidentURL(): URL {
    const url = new URL(this.config.get<string>('K8S_SECRET_ITSM_URL'));
    url.pathname = '/api/now/table/incident';
    return url;
  }

  get authURL(): URL {
    const authURL = new URL(this.incidentURL.toString());
    authURL.username = this.config.get<string>('K8S_SECRET_ITSM_USER');
    authURL.password = this.config.get<string>('K8S_SECRET_ITSM_PW');
    return authURL;
  }

  async createTicket(createTicket: CreateTicket): Promise<Ticket> {
    try {
      const {
        data: { result },
      } = await this.http
        .post<{ result: ServiceNowTicket }>(
          this.authURL.toString(),
          {
            short_description: createTicket.shortDescription,
            description: createTicket.description,
          },
          {
            headers: {
              accept: 'application/json',
            },
          },
        )
        .toPromise();
      return new Ticket(
        result.sys_id,
        result.short_description,
        result.description,
        result.number,
        Number(result.state),
        new Date(result.opened_at),
      );
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  async updateTicket(id: string, updateTicket: UpdateTicket): Promise<Ticket> {
    try {
      const url = this.authURL;
      url.pathname += `/${id}`;

      const {
        data: { result },
      } = await this.http
        .put<{ result: ServiceNowTicket }>(
          url.toString(),
          {
            ...updateTicket.shortDescription && {short_description: updateTicket.shortDescription},
            ...updateTicket.description && {description: updateTicket.description},
          },
          {
            headers: {
              accept: 'application/json',
            },
          },
        )
        .toPromise();
      return new Ticket(
        result.sys_id,
        result.short_description,
        result.description,
        result.number,
        Number(result.state),
        new Date(result.opened_at),
      );
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  async getTicket(id:string): Promise<Ticket> {
    try {
      const url = this.authURL;
      url.pathname += `/${id}`;
      const {
        data: { result },
      } = await this.http.get<{ result: ServiceNowTicket }>(url.toString(), {
        headers: {
          accept: 'application/json',
        },
      })
        .toPromise();

      return new Ticket(
        result.sys_id,
        result.short_description,
        result.description,
        result.number,
        Number(result.state),
        new Date(result.opened_at),
      );
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  async getAllTickets(filters?: {
    number?: string
    startDate?: Date,
    endDate?: Date,
    state?: number,
  }): Promise<Ticket[]> {
    try {
      const res = await this.http
        .get<{ result: ServiceNowTicket[] }>(this.authURL.toString(), {
          headers: {
            accept: 'application/json',
          },
        })
        .toPromise();
      const tickets =  res.data.result.map(
        (t) =>
          new Ticket(
            t.sys_id,
            t.short_description,
            t.description,
            t.number,
            Number(t.state),
            new Date(t.opened_at)),
      );

      if (!filters) {
        return tickets;
      }

      return tickets.filter(t => {
        let keep = true;

        if (filters.number) {
          keep = keep && (t.number.startsWith(filters.number));
        }

        if (filters.startDate) {
          keep = keep && (t.openedAt.valueOf() >= filters.startDate.valueOf());
        }

        if (filters.endDate) {
          keep = keep && (t.openedAt.valueOf() <= filters.endDate.valueOf());
        }

        if (filters.state) {
          keep = keep && (t.state === filters.state);
        }

        return keep;
      });
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  async deleteTicket(id: string): Promise<void> {
    try {
      const url = this.authURL;
      url.pathname += `/${id}`;
      await this.http.delete(url.toString(), {
          headers: {
            accept: 'application/json',
          },
        })
        .toPromise();
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
}
