import { Kentan } from '@kentan-official/core';
import { HttpService } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { ForCreateTicket } from '@src/ticket/model/ForCreateTicket.sketch';
import { ForServiceNowTicket } from '@src/ticket/model/ForServiceNowTicket.sketch';
import { ForTicket } from '@src/ticket/model/ForTicket.sketch';
import { Ticket } from '@src/ticket/model/Ticket.dto';
import { AxiosResponse } from 'axios';
import { Observable, of, throwError } from 'rxjs';
import { SinonStub, SinonStubbedInstance, stub } from 'sinon';
import { ServiceNowService } from './service-now.service';

function toHttpResponse<T>(response: T): Observable<AxiosResponse<T>> {
  return of(({ data: { result: response } } as unknown) as AxiosResponse<T>);
}

describe('ServiceNowService', () => {
  let module: TestingModule;
  let httpService: SinonStubbedInstance<HttpService>;
  let service: ServiceNowService;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      providers: [
        ServiceNowService,
        {
          provide: HttpService,
          useFactory: function () {
            return stub(new HttpService());
          },
        },
        {
          provide: ConfigService,
          useFactory: function () {
            return stub(new ConfigService());
          },
        },
      ],
    }).compile();
  });

  beforeEach(() => {
    const configService = module.get<
      ConfigService,
      SinonStubbedInstance<ConfigService>
    >(ConfigService);
    (configService.get as SinonStub<string[], any>)
      .withArgs('K8S_SECRET_ITSM_URL')
      .returns('https://www.example.com');
    (configService.get as SinonStub<string[], any>)
      .withArgs('K8S_SECRET_ITSM_USER')
      .returns('USER');
    (configService.get as SinonStub<string[], any>)
      .withArgs('K8S_SECRET_ITSM_PW')
      .returns('PASSWORD');

    httpService = module.get<HttpService, SinonStubbedInstance<HttpService>>(
      HttpService,
    );

    service = module.get<ServiceNowService>(ServiceNowService);
  });

  it('should be defined', () => {
    // system under test
    const service = module.get<ServiceNowService>(ServiceNowService);

    expect(service).toBeDefined();
  });

  describe('create ticket', () => {
    it('should return the created ticket', async () => {
      // arrange
      const createTicket = Kentan.sketch(ForCreateTicket).model();
      const responseTicket = Kentan.sketch(ForServiceNowTicket).model({
        description: createTicket.description,
        short_description: createTicket.shortDescription,
      });
      const resultTicket = new Ticket(
        responseTicket.sys_id,
        responseTicket.short_description,
        responseTicket.description,
        responseTicket.number,
        Number(responseTicket.state),
        new Date(responseTicket.opened_at),
      );

      httpService.post.returns(toHttpResponse(responseTicket));

      // act
      const result = await service.createTicket(createTicket);

      // assert
      expect(result).toEqual(resultTicket);
      expect(httpService.post.lastCall.args[0]).toEqual(
        'https://USER:PASSWORD@www.example.com/api/now/table/incident',
      );
      expect(httpService.post.lastCall.args[1]).toEqual({
        short_description: createTicket.shortDescription,
        description: createTicket.description,
      });
    });

    it('should propagate ocuring errors', async () => {
      // arrange
      const createTicket = Kentan.sketch(ForCreateTicket).model();
      const error = new Error('Test');

      httpService.post.returns(throwError(error));

      // act
      try {
        const result = await service.createTicket(createTicket);
        fail('should throw');
      } catch (e) {
        // assert
        expect(e).toEqual(error);
      }
    });
  });

  describe('get all tickets', () => {
    it('should return all tickets', async () => {
      // arrange
      const responseTickets = Kentan.sketch(ForServiceNowTicket)
        .take(5)
        .models();
      const resultTickets = responseTickets.map(
        (t) =>
          new Ticket(t.sys_id, t.short_description, t.description, t.number, Number(t.state), new Date(t.opened_at)),
      );
      httpService.get.returns(toHttpResponse(responseTickets));

      // act
      const result = await service.getAllTickets();

      // assert
      expect(result).toEqual(resultTickets);
      expect(httpService.get.lastCall.args[0]).toEqual(
        'https://USER:PASSWORD@www.example.com/api/now/table/incident',
      );
    });

    it('should propagate ocuring errors', async () => {
      // arrange
      const error = new Error('Test');

      httpService.get.returns(throwError(error));

      // act
      try {
        const result = await service.getAllTickets();
        fail('should throw');
      } catch (e) {
        // assert
        expect(e).toEqual(error);
      }
    });
  });
});
