import { Controller, Get } from '@nestjs/common';
import { hostname } from 'os';
import {resolve} from 'path'

@Controller('system')
export class SystemController {
  @Get('info')
  getInfo(): {hostname: string, version: string} {
    const p = require(resolve(__dirname, '..', '..', '..', 'package.json'));
    return {
      hostname: hostname(),
      version: p.version,
    }
  }
}
