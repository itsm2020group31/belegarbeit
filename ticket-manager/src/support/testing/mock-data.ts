import * as Chance from 'chance';

// use a constant seed to have a reproducible test data
const SEED = 'eruighaeruiogbuioergae';

export const MockData: Chance.Chance = new Chance(SEED);
