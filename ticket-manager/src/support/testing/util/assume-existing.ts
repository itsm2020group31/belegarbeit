/**
 * Testing helper which ensures that a optional value is actually present
 * @param v The value to test
 * @returns The value v is if is truthy
 */
export function assumeExisting<T>(v: T | undefined | null): T {
  if (!v) {
    throw new Error('Value is not defined');
  }

  return v;
}

export const exists = assumeExisting;
