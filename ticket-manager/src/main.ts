import { INestApplication, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { ServeStaticOptions } from '@nestjs/platform-express/interfaces/serve-static-options.interface';
import { AppModule } from '@src/app.module';
import { join } from 'path';
import hbs = require('hbs');
import hbsHelpers = require('handlebars-helpers');

export interface ViewConfigurable {
  useStaticAssets?(options: ServeStaticOptions): this;

  /**
   * Sets a base directory for public assets.
   * @example
   * app.useStaticAssets('public')
   *
   * @returns {this}
   */
  useStaticAssets?(path: string, options?: ServeStaticOptions): this;

  /**
   * Sets one or multiple base directories for templates (views).
   *
   * @example
   * app.setBaseViewsDir('views')
   *
   * @returns {this}
   */
  setBaseViewsDir?(path: string | string[]): this;

  /**
   * Sets a view engine for templates (views).
   * @example
   * app.setViewEngine('pug')
   *
   * @returns {this}
   */
  setViewEngine?(engine: string): this;
}

export function bootstrapView<T extends ViewConfigurable>(app: T): T {
  hbs.registerPartials(join(__dirname, '..', 'views/partials'));
  hbsHelpers({hbs});
  app.setBaseViewsDir?.(join(__dirname, '..', 'views'));
  app.setViewEngine?.('hbs');
  app.useStaticAssets?.(join(__dirname, '..', 'public'));
  return app;
}

export function bootstrapApplication<T extends INestApplication>(app: T): T {
  app.useGlobalPipes(new ValidationPipe());
  return app;
}

async function start() {
  await bootstrapView(
    bootstrapApplication(
      await NestFactory.create<NestExpressApplication>(AppModule),
    ),
  ).listen(3000);
}

start();
