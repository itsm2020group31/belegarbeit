import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from '@src/app.controller';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  it('should be defined', () => {
    expect(appController).toBeDefined();
  });

  describe('root', () => {
    it('should not pass any parameters to the template', () => {
      expect(appController.getIndex()).toEqual({});
    });
  });
});
