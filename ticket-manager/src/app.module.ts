import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from '@src/app.controller';
import { TicketModule } from '@src/ticket/ticket.module';
import { SystemModule } from './system/system.module';
import * as Joi from 'joi';

@Module({
  imports: [
    TicketModule,
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        K8S_SECRET_ITSM_URL: Joi.string().uri().exist(),
        K8S_SECRET_ITSM_USER: Joi.string().min(1).exist(),
        K8S_SECRET_ITSM_PW: Joi.string().min(1).exist(),
      }),
    }),
    SystemModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
