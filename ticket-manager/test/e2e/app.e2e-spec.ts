import { Test, TestingModule } from '@nestjs/testing';
import { HttpServer, INestApplication } from '@nestjs/common';
import { bootstrapApplication, bootstrapView } from '@src/main';
import * as request from 'supertest';
import { AppModule } from '@src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    bootstrapView(app.getHttpAdapter());
    bootstrapApplication(app);

    await app.listen(3001);
  });

  afterEach(async () => {
    await app.close();
  });

  it('Should answer successful on / (GET)', () => {
    return request(app.getHttpServer()).get('/').expect(200);
  });

  describe('validation', () => {
    it('Should return 400 on invalid input)', () => {
      return request(app.getHttpServer())
        .post('/ticket')
        .field('name', '') // name is a required property
        .expect(400);
    });
  });
});
