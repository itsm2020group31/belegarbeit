import { check } from 'k6';
import http from 'k6/http';

const host = __ENV.HOST || 'localhost'

export const options = {
    thresholds: {
        checks: ['rate>=1']
    },
};

export default function () {
    const res = http.get(`http://${host}:3000/`);
    check(res, {
        'is status 200': r => r.status === 200,
        'has "Ticket Manager" heading': r => r.body && r.body.includes('<h1>Ticket Manager</h1>'),
        'contains button to new Ticket': r => r.body && r.body.includes('<button type="submit">Create</button>'),
    })
}