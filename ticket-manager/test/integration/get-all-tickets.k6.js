import { check } from 'k6';
import http from 'k6/http';

const host = __ENV.HOST || 'localhost'

export const options = {
    thresholds: {
        checks: ['rate>=1']
    },
};

export default function () {
    const res = http.get(`http://${host}:3000/ticket`);
    check(res, {
        'is status 200': r => r.status === 200,
        'has "Tickets" heading': r => r.body && r.body.includes('<h1>Tickets</h1>'),
        'contains tickets': r => r.body && r.body.includes('<td>INC'),
    })
}