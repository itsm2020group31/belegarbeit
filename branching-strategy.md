# Branching Strategy
To homogenize the workflow in the project and apply appropriate project governance methods, the following branching strategy is used inside the project.

## Work Items
All work done in the projects needs to be tracked as an work item in the [GitLab Board](https://gitlab.com/itsm2020group31/belegarbeit/-/boards).
Each item in the board has a uniqe id, which can be used to identify the item and reference it.

## Branches
Name | Pattern | Example | Purpose | Branched from | Update Method | Merged to | Quality Level | Lifecycle
--- | --- | --- | --- | --- | --- | --- | --- | ---
Mainline | `master` | `master` | Mainline storing the current releasable state of the project | - | Merge Request | - | Fully working, quality assured and releasable | Durable
Feature Branch | `feature/\<ISSUE ID\>-short_description`| `feature/5-document_branching_strategy` | Active development of a new functionality | master | Merge Request (in case of task branches) or direct commit | master | Work in development (functional issues might exits, quality assurance might be incomplete or faulty) | Active during the active development of the feature. Ends with merge to Mainline
Bugfix Branch | `bugfix/\<ISSUE ID\>-short_description`| `bugfix/12-fix_bluescreen_error` | Active fixing of a bug in an existing functionality | master | Merge Request (in case of task branches) or direct commit | master | Work in development (functional issues might exits, quality assurance might be incomplete or faulty) | Active during the active fixing of the bug. Ends with merge to Mainline
Task Branch | `task/\<ISSUE ID\>/short_task_description`| `task/5/document_branch_types` | Active development of smaller increments of a new functionality or bugfix | Related Feature/Bugfix Branch | Direct commit | Related Feature/Bugfix Branch | Work in development (functional issues might exits, quality assurance might be incomplete or faulty) | Active during the active development of the increment. Ends with merge to the Feature Branch

## Updating branches
A branch can be updated either via direct commits and pushes to the branch or by integrating another branch into the target branch.

All integrations from a lower level branch to a higher level branch (e.g. Task Branch to Feature Branch, Feature Branch to Mainline) should happen via [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/) instead of direct pushes.
The Merge Requests needs to be reviewed by another project contributer and needs to be quality assured by running a Continuous Integration Build.

The Mainline, as the highest level branch, can **only** be updated via integrating a lower level branch and should not be changed by direct pushing of changes. This is ensured via a [Branch Protection](https://docs.gitlab.com/ee/user/project/protected_branches.html).

## Commit Message
When Updating a branch via a direct commit the following pattern for the commit message should be used:

```
<ISSUE ID> Short description of the change

[Optional longer explanation and reason of the change]
```

Example:

```
5 Document branching strategy

To homogenize the branching strategy and commit history, a branching stratgy is documented as Markdown document in the project root.
```

## Release Tags
All versions of the software which should be released to the production environment are tagged with a corresponding git Tag on the Mainline. The Version numbers follow the [Semantic Versioning Standard](https://semver.org/lang/en/).
