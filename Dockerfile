FROM node:10
WORKDIR /app
COPY ./ticket-manager/dist ./bin
COPY ./ticket-manager/views ./views
COPY ./ticket-manager/node_modules ./node_modules
EXPOSE 3000
CMD ["node", "bin/main"]

